import Vue from 'vue'
import Vuex from 'vuex'
import * as actions from './actions'
// import * as getters from './getters'
import requireApi from '../api/requireApi'
import mutations from './mutations'
// 生成保存API请求数据对象默认值
const APIREQUESTDATA = {}
for (let i in requireApi) {
  APIREQUESTDATA[i] = null
}
Vue.use(Vuex)
const state = {
  newOne: '新增mutation示例',
  API: APIREQUESTDATA
}
export default new Vuex.Store({
  state,
  mutations,
  actions
})
