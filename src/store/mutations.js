import requireApi from '../api/requireApi'
let mutation = {}
// 遍历生成API MUTATION 对象函数
for (let api in requireApi) {
  mutation[api] = (state, res) => {
    console.log('执行记录用户信息')
    state.API[api] = res.data
  }
}
mutation = Object.assign({}, mutation, {
  newOne: (state, data) => {
    state.newOne = data
  }
})
export default mutation
