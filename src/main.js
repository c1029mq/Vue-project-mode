// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import config from './config'
import require from './api/require'
import store from './store/index'
/* eslint-disable no-new */
new Vue({
  el: '#app',
  data () {
    return {
      config: config
    }
  },
  methods: require,
  router,
  store,
  components: { App },
  template: '<App/>'
})
