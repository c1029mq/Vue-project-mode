import axios from './http'
import config from '../config'
const URI = config.DEBUG ? '192.168.1.126' : window.location.origin
const JSONDATA = (postData) => {
  if (postData !== undefined) {
    let postDataStr = JSON.stringify(postData)
    postData = JSON.parse(postDataStr)
  }
  var oData = {
    data: {
    }
  }
  if (postData instanceof Object) {
    for (let x in postData) {
      oData.data[x] = postData[x]
    }
  }
  return oData.data
}

/**
 * 以get方式抓取远端API的结构
 * https://developers.douban.com/wiki/?title=movie_v2
 * @param  {String} path   请求路径
 * @param  {Objece} params 参数
 * @return {Promise}       包含抓取任务的Promise
 */
const fetchGET = (path, params) => {
  params = JSONDATA(params)
  return axios.get(`${URI}/${path}`, { params })
    .then(res => {
      console.log('请求成功')
      return res
    })
    .catch(e => {
      console.log('网络出错')
    })
}
const fetchPOST = (path, params) => {
  params = JSONDATA(params)
  return axios.get(`${URI}/${path}`, { params })
    .then(res => {
      console.log('请求成功')
      return res
    })
    .catch(e => {
      console.log('网络出错')
    })
}
export default {
  fetchPOST,
  fetchGET
}
