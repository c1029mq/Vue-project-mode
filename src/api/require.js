import requireApi from './requireApi'
import store from '../store'
const APIFUNCTION = {}
for (let api in requireApi) {
  APIFUNCTION[api] = (param) => {
    return requireApi[api](param).then(res => {
      console.log(res)
      if (res.data.status && res.data.status === 200) {
        console.log('成功')
        store.commit(api, res)
      } else {
        console.log('失败')
        store.commit(api, res)
      }
    }).catch(res => {
      console.log(res)
      console.log('获取请求日志报错信息: ' + res)
    })
  }
}
export default APIFUNCTION
