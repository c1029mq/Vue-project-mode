/**
 * http配置
 */
import axios from 'axios'

// 超时时间
axios.defaults.timeout = 5000
// // http请求拦截器
var loadinginstace
axios.interceptors.request.use(config => {
  console.log(config)
  return config
}, error => {
  loadinginstace.close()
  console.log('加载超时')
  return Promise.reject(error)
})
// http响应拦截器
axios.interceptors.response.use(data => { // 响应成功关闭loading
  return Promise.resolve(data)
}, error => {
  loadinginstace.close()
  console.log('加载失败')
  return Promise.reject(error)
})
export default axios
